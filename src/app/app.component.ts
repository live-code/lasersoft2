import { Component } from '@angular/core';

@Component({
  selector: 'ls-root',
  template: `
    
    <ls-navbar></ls-navbar>
    <router-outlet></router-outlet>
    
  `,
  styles: [`
    .active {
      background-color: blueviolet;
    }
  `]
})
export class AppComponent {
  title = 'lasersoft2';
}
