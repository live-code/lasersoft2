import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Auth } from './auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  data: Auth | null = null;

  constructor(private http: HttpClient) {
    let auth: string | null = localStorage.getItem('auth');

    if (auth) {
      this.data = JSON.parse(auth)
    }
  }

  login(username: string, password: string): Promise<any> {
    return new Promise((resolve, rejected) => {
      const params = new HttpParams()
        .set('user', username)
        .set('pass', password)

      this.http.get<Auth>('http://localhost:3000/login', { params })
        .subscribe(
          res => {
            this.data = res;
            localStorage.setItem('auth', JSON.stringify(res))
            resolve(res)
          },
          err => {
            rejected(err.status)
          }
        )
    })
  }

  logout() {
    this.data = null;
    localStorage.removeItem('auth')
  }

  isLogged(): boolean {
    return !!this.data
  }

  showIfRoleIs(role: string) {
    return role === this.data?.role
  }

}
