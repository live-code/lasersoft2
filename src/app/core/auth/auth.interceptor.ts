import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newReq = req;
    const tk: string | undefined = this.authenticationService.data?.token;
    // Bearer oiewfofo2fowefljwel
    if (this.authenticationService.isLogged() && tk) {
      newReq = req.clone({
        setHeaders: {
          Authorization: tk
        }
      })
    }
    return next.handle(newReq)
      .pipe(
        catchError(err => {
          switch(err.status) {
            case 401:
              console.log('token expired')
              break;
            case 404:
            default:
              console.log('redirect to login')
              this.router.navigateByUrl('login')
              break;
          }
          return throwError(err)
        })
      )
  }

}
