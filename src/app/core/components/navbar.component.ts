import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Component({
  selector: 'ls-navbar',
  template: `
    <button routerLink="home" routerLinkActive="active">home</button>
    <button routerLink="admin" routerLinkActive="active" *ngIf="authenticationService.showIfRoleIs('admin')">admin</button>
    <button routerLink="login" routerLinkActive="active">login</button>
    <button routerLink="settings" routerLinkActive="active"  *ngIf="authenticationService.isLogged()">settings</button>
    <button routerLink="catalog" routerLinkActive="active">catalog</button>
    <button
      *ngIf="authenticationService.isLogged()"
      (click)="authenticationService.logout()">Logout</button>
    
    <!--{{authenticationService.data?.displayName}}-->
    
    <ng-container *ngIf="authenticationService.data">
      {{authenticationService.data.displayName}}
    </ng-container>
    <hr>

  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor(public authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }

}
