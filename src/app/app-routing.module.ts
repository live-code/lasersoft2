import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)},
      { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
      { path: 'home', loadChildren: () => import('./features/homepage/homepage.module').then(m => m.HomepageModule) },
      { path: 'settings', canActivate: [ AuthGuard ], loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule) },
      { path: 'admin', canActivate: [ AuthGuard ], loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
      { path: '**', redirectTo: 'login'}
    ]),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
