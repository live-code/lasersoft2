import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ls-catalog',
  template: `
    <p>
      catalog works!
    </p>

    <ls-card></ls-card>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
