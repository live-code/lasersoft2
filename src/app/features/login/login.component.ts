import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ls-login',
  template: `
    
    <router-outlet></router-outlet>
    <hr>
    
    <button routerLink="signin" routerLinkActive="active">signin</button>
    <button routerLink="/login/registration" routerLinkActive="active">registration</button>
    <button routerLink="/login/lostpass" routerLinkActive="active">lostpass</button>
  `,
  styles: [`
    .active {
      background-color: blueviolet;
    }
  `]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
