import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './component/signin.component';
import { RegistrationComponent } from './component/registration.component';
import { RouterModule } from '@angular/router';
import { LostpassComponent } from './component/lostpass.component';

@NgModule({
  declarations: [
    LoginComponent,
    SigninComponent,
    RegistrationComponent,
    LostpassComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([

      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', component: SigninComponent },
          { path: 'lostpass', component: LostpassComponent },
          { path: 'registration', component: RegistrationComponent },
          { path: '', redirectTo: 'signin'}
        ]
      }
    ])
  ]
})
export class LoginModule {}
