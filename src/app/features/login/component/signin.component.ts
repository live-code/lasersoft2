import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../core/auth/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ls-signin',
  template: `
    <h1>Sign In</h1>
    <pre>{{authenticationService.data | json}}</pre>
    <form #f="ngForm" (submit)="signInHandler(f.value)">
      <input type="text" name="user" [ngModel]>
      <input type="text" name="pass" [ngModel]>
      <button type="submit">LOGIN</button>
    </form>
  `,
})
export class SigninComponent {

  constructor(
    public authenticationService: AuthenticationService,
    private router: Router
  ) { }

  // signInHandler(data: { user: string, pass: string }) {
  signInHandler(data: { user: string, pass: string}) {
    this.authenticationService.login(data.user, data.pass)
      .then((res) => {
        this.router.navigateByUrl('home')
      })
      .catch(err => {
        console.log('errore: ', err)
      })
      // ...
      // this.router.navigateByUrl('home')
  }
}
