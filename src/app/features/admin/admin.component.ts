import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../core/auth/authentication.service';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'ls-admin',
  template: `
    <p>
      admin works!
    </p>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(private http: HttpClient) {
    const sub = http.get('http://localhost:3000/movements')
      .subscribe(
        res => {
          console.log('res', res)
        },
        err => {
          console.log('errore!')
        }
      )
  }

  ngOnInit(): void {
  }

}
