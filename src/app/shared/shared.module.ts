import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card.component';
import { HelloComponent } from './hello.component';
import { PanelComponent } from './panel.component';



@NgModule({
  declarations: [
    CardComponent,
    HelloComponent,
    PanelComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    HelloComponent,
    PanelComponent
  ]
})
export class SharedModule { }
